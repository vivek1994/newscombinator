package models

import "time"

type LoginCredential struct {
 Mobile     string  `json:"mobile" bson:"mobile"`
 Password     string  `json:"password" bson:"password"`
}
type PayloadForListOfNews struct {
	Id     string    `json:"_id" bson:"_id"`
	Skip     int  `json:"skip" bson:"skip"`
}

type UserSignup struct {
	Id     string    `json:"_id" bson:"_id"`
	Name     string  `json:"name" bson:"name"`
	Status     string  `json:"status" bson:"status"`
	Mobile     string  `json:"mobile" bson:"mobile"`
	Password     string  `json:"password" bson:"password"`
	CreatedOn  time.Time   `json:"createdOn" bson:"createdOn"`
	NewsUnLiked   []string    `json:"newsUnLinked" bson:"newsUnLinked"`
}

type NewsList struct {
	Id     string    `json:"_id" bson:"_id"`
        Url    string     `json:"url" bson:"url"`
	Title  string   `json:"title" bson:"title"`
	Text  string   `json:"text" bson:"text"`
	Status     string  `json:"status" bson:"status"`
	CreatedOn  time.Time   `json:"createdOn" bson:"createdOn"`
	CreatedBy  string   `json:"createdBy" bson:"createdBy"`
	Points      int     `json:"points" bson:"points"`
	PointsGivenBy []string `json:"pointsGivenBy" bson:"pointsGivenBy"`
	Unliked []string `json:"Unliked" bson:"Unliked"`
}
type CommentList struct {
	Id     string    `json:"_id" bson:"_id"`
	Status     string  `json:"status" bson:"status"`
	CommentList []Comments `json:"commentList" bson:"commentList"`
}
type Comments struct {
	Id     string    `json:"_id" bson:"_id"`
	CreatedOn  time.Time   `json:"createdOn" bson:"createdOn"`
	CreatedBy  string   `json:"createdBy" bson:"createdBy"`
	Status     string  `json:"status" bson:"status"`
	Text  string   `json:"text" bson:"text"`
	ListOfReply  []Reply  `json:"listOfReply" bson:"listOfReply"`
}
type Reply struct {
	Id     string    `json:"_id" bson:"_id"`
	CreatedOn  time.Time   `json:"createdOn" bson:"createdOn"`
	CreatedBy  string   `json:"createdBy" bson:"createdBy"`
	Text  string   `json:"text" bson:"text"`
	Status     string  `json:"status" bson:"status"`
}