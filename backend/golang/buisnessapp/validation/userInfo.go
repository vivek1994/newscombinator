package validation

import (
	"github.com/backend/golang/buisnessapp/models"
	"github.com/backend/golang/common/system"
)
func ValidateSignupData(userData models.UserSignup)error{
	if userData.Mobile==""{
		return system.MobileBlankErr
	}else if userData.Password==""{
		return system.PasswordBlankErr
	}else if userData.Name==""{
		return system.NameBlankErr
	}
	return nil
}
