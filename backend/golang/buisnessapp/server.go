package main

import (
	"github.com/backend/golang/common/system"
	"github.com/backend/golang/buisnessapp/routes"
	"github.com/zenazn/goji"
	"github.com/rs/cors"
)


func main(){
	var application = &system.Application{}
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
	})
	goji.Use(c.Handler)
	routes.PrepareRoutes(application)
	goji.Serve()
}

