package routes

import (
	"github.com/backend/golang/common/system"
	"github.com/backend/golang/buisnessapp/api"
	"github.com/zenazn/goji"
)


func PrepareRoutes(application *system.Application){
	goji.Post("/businessapp/controller/user/login",application.Routes(&api.Controller{},"UserLogin","public"))
	goji.Post("/businessapp/controller/user/signup",application.Routes(&api.Controller{},"UserSignUp","public"))
	goji.Post("/businessapp/controller/user/addnews",application.Routes(&api.Controller{},"AddNews","private"))
	goji.Post("/businessapp/controller/user/news/list",application.Routes(&api.Controller{},"ListOfNews","public"))
	goji.Post("/businessapp/controller/user/news/comment",application.Routes(&api.Controller{},"AddComment","private"))
	goji.Post("/businessapp/controller/user/news/comment/reply",application.Routes(&api.Controller{},"AddReply","private"))
	goji.Post("/businessapp/controller/user/news/like",application.Routes(&api.Controller{},"UnlikedNews","private"))
}