package api

import (
        "github.com/backend/golang/common/system"
        "github.com/backend/golang/buisnessapp/services"
	"github.com/zenazn/goji/web"
	"net/http"
	"encoding/json"
	"github.com/backend/golang/buisnessapp/models"
	"fmt"
	"strings"
)

type Controller struct {
	system.Controller
}

func(controller *Controller) UserSignUp(c web.C, w http.ResponseWriter, r *http.Request) ([]byte, error){
	decoder := json.NewDecoder(r.Body)
	var loginInput models.UserSignup
	err := decoder.Decode(&loginInput)
	if err != nil {
		return nil, err
	}
	response, err := services.UserSignUp(loginInput)

	if (err != nil) {
		return nil, err
	}
	return response, err

}
func(controller *Controller) UserLogin(c web.C, w http.ResponseWriter, r *http.Request) ([]byte, error){
	decoder := json.NewDecoder(r.Body)
	var loginInput models.LoginCredential
	err := decoder.Decode(&loginInput)
	if err != nil {
		return nil, err
	}
	response, err := services.UserLogin(loginInput)

	if (err != nil) {
		return nil, err
	}
	return response, err

}

func(controller *Controller) AddNews(c web.C, w http.ResponseWriter, r *http.Request) ([]byte, error){
	AccessKey:=r.Header["Authorization"]
	fmt.Println("accesskey ",AccessKey)
	authToken := strings.Split(AccessKey[0], "bearer")[1]
	fmt.Println("accesskey ",authToken)
	key :=system.RemoveSpace(authToken)
	decoder := json.NewDecoder(r.Body)
	var newsInfo models.NewsList
	err := decoder.Decode(&newsInfo)
	if err != nil {
		return nil, err
	}
	response, err := services.AddNews(newsInfo,key)

	if (err != nil) {
		return nil, err
	}
	return response, err

}



func(controller *Controller) ListOfNews(c web.C, w http.ResponseWriter, r *http.Request) ([]byte, error){
	decoder := json.NewDecoder(r.Body)
	var newsInfo models.PayloadForListOfNews
	err := decoder.Decode(&newsInfo)
	if err != nil {
		return nil, err
	}
	response, err := services.ListOfNews(newsInfo.Id,newsInfo.Skip)

	if (err != nil) {
		return nil, err
	}
	return response, err

}
func(controller *Controller) UnlikedNews(c web.C, w http.ResponseWriter, r *http.Request) ([]byte, error){
	AccessKey:=r.Header["Authorization"]
	fmt.Println("accesskey ",AccessKey)
	authToken := strings.Split(AccessKey[0], "bearer")[1]
	fmt.Println("accesskey ",authToken)
	key :=system.RemoveSpace(authToken)
	decoder := json.NewDecoder(r.Body)
	var newsInfo map[string]string
	err := decoder.Decode(&newsInfo)
	if err != nil {
		return nil, err
	}
	newsId:=newsInfo["newsId"]
	response, err := services.UnlikedNews(key,newsId)
	if (err != nil) {
		return nil, err
	}
	return response, err

}

func(controller *Controller) AddComment(c web.C, w http.ResponseWriter, r *http.Request) ([]byte, error){
	decoder := json.NewDecoder(r.Body)
	var commentInfo models.CommentList
	err := decoder.Decode(&commentInfo)
	if err != nil {
		return nil, err
	}
	response, err := services.AddComment(commentInfo)

	if (err != nil) {
		return nil, err
	}
	return response, err

}

func(controller *Controller) AddReply(c web.C, w http.ResponseWriter, r *http.Request) ([]byte, error){
	decoder := json.NewDecoder(r.Body)
	var commentInfo models.CommentList
	err := decoder.Decode(&commentInfo)
	if err != nil {
		return nil, err
	}
	response, err := services.AddComment(commentInfo)

	if (err != nil) {
		return nil, err
	}
	return response, err

}