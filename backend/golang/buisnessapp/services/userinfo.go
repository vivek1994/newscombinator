package services

import(
         "github.com/backend/golang/buisnessapp/models"
	"github.com/backend/golang/common/system"
	"encoding/json"
	"gopkg.in/mgo.v2/bson"
	"fmt"
	"github.com/backend/golang/buisnessapp/validation"
	"github.com/pborman/uuid"
	"time"
)

func UserSignUp(loginData models.UserSignup)([]byte,error){
	var loginInfo []models.LoginCredential
	//validating required field
	err:=validation.ValidateSignupData(loginData)
	if err!=nil{
		return nil,err
	}

	db:=system.UDB.C(system.USER_COLLECTION)
	//checking user already exist or not
	err=db.Find(bson.M{"mobile":loginData.Mobile}).All(&loginInfo)
	if err!=nil{
		return nil,err
	}
	if len(loginInfo)>0{
		return nil,system.UserAlreadyExistErr
	}
        loginData.Id=uuid.New()
        loginData.CreatedOn=time.Now()
	loginData.Status=system.ACTIVE
	err=db.Insert(&loginData)
	if err!=nil{
		return nil,err
	}

	response:=make(map[string]interface{})
	response["message"]="registration Successfull"
	finalResponse, _ := json.Marshal(response)
	return finalResponse, nil
}

func UserLogin(loginData models.LoginCredential)([]byte,error){
	fmt.Println("user validation ",loginData)
	var loginInfo []models.UserSignup
	db:=system.UDB.C(system.USER_COLLECTION)
	err:=db.Find(bson.M{"mobile":loginData.Mobile,"password":loginData.Password}).All(&loginInfo)
	if err!=nil{
		return nil,err
	}
	fmt.Print("datataaaa")
	if len(loginInfo)==0{
		return nil,system.InvalidLoginErr
	}
	response:=make(map[string]interface{})
	response["message"]="login Successfull"
	response["userinfo"]=loginInfo[0]
	finalResponse, _ := json.Marshal(response)
	return finalResponse, nil
}
func AddNews(newsInfo models.NewsList,userId string)([]byte,error){
	db:=system.UDB.C(system.NEWS)
	newsInfo.Id=uuid.New()
	newsInfo.CreatedOn=time.Now()
	newsInfo.CreatedBy=userId
	newsInfo.Status=system.ACTIVE

	err:=db.Insert(&newsInfo)
	if err!=nil{
		return nil,err
	}
	response:=make(map[string]interface{})
	response["message"]="News Addedd Suucessfully"
	finalResponse, _ := json.Marshal(response)
	return finalResponse, nil
}
func ListOfNews( userId string, skip int)([]byte,error){
	fmt.Println("userId11111111111",userId)
	db:=system.UDB.C(system.NEWS)
	var newsInfo []map[string]interface{}
	var newsInfo1 []map[string]interface{}
	var IsMoreList bool
	err:=db.Find(nil).All(&newsInfo1)
	if err!=nil{
		return nil,err
	}
	if len(newsInfo1)==0{
		return nil,system.NoDataFoundErr
	}
	skip=8*skip
	if len(newsInfo1)>skip+8{
		IsMoreList=true;
	}
	var loginInfo []models.UserSignup
	err=db.Find(nil).Sort("createdOn").Skip(skip).Limit(8).All(&newsInfo)
	if err!=nil{
		return nil,err
	}
	if userId !="" {
		db:=system.UDB.C(system.USER_COLLECTION)
		err = db.Find(bson.M{"_id":userId, "status":system.ACTIVE}).All(&loginInfo)
		if err != nil {
			return nil, err
		}
		fmt.Print("datataaaa")
		unlikedNews := make(map[string]interface{})
		if len(loginInfo) == 0 {
			return nil, system.InvalidLoginErr
		} else {
			for i := 0; i < len(loginInfo[0].NewsUnLiked); i++ {
				unlikedNews[loginInfo[0].NewsUnLiked[i]] = i
			}
		}
		for i := 0; i < len(newsInfo); i++ {
			if unlikedNews[newsInfo[i]["_id"].(string)] != nil {
				newsInfo[i]["likeFlag"] = true;
			} else {
				newsInfo[i]["likeFlag"] = false;
			}
		}
	}else{
		for i := 0; i < len(newsInfo); i++ {

				newsInfo[i]["likeFlag"] = false
		}
	}
	fmt.Println("data is",newsInfo)
	response:=make(map[string]interface{})
	response["listOfNews"]=newsInfo
	response["IsMoreList"]=IsMoreList
	finalResponse, _ := json.Marshal(response)
	return finalResponse, nil
}
func AddComment(commentInfo  models.CommentList)([]byte,error){
	db:=system.UDB.C(system.COMMENTS)
	commentInfo.CommentList[0].Id=uuid.New()
	commentInfo.CommentList[0].CreatedOn=time.Now()
	commentInfo.CommentList[0].Status=system.ACTIVE
	if commentInfo.Id !=""{
		where:=bson.M{"_id":commentInfo.Id,"status":system.ACTIVE}
		update:=bson.M{"$push":bson.M{"commentList":commentInfo.CommentList[0]}}
		err:=db.Update(where,update)
		if err!=nil{
			return nil,err
		}
	}else{
		commentInfo.Id=uuid.New()
		commentInfo.Status=system.ACTIVE
		err:=db.Insert(&commentInfo)
		if err!=nil{
			return nil,err
		}
	}
	response:=make(map[string]interface{})
	response["message"]="comment Added Successfully"
	finalResponse, _ := json.Marshal(response)
	return finalResponse, nil
}
func AddReply(commentInfo  models.CommentList)([]byte,error) {
	db := system.UDB.C(system.COMMENTS)
	commentInfo.CommentList[0].ListOfReply[0].Id = uuid.New()
	commentInfo.CommentList[0].ListOfReply[0].Status = system.ACTIVE
	commentInfo.CommentList[0].ListOfReply[0].CreatedOn = time.Now()
	if commentInfo.Id != "" {
		where := bson.M{"_id":commentInfo.Id, "status":system.ACTIVE, "commentList":bson.M{"$elemMatch":bson.M{"_id":commentInfo.CommentList[0].Id}}}
		update := bson.M{"$push":bson.M{"reply":commentInfo.CommentList[0].ListOfReply[0]}}
		err := db.Update(where, update)
		if err != nil {
			return nil, err
		}
	}
	response := make(map[string]interface{})
	response["message"] = "replly Added Successfully"
	finalResponse, _ := json.Marshal(response)
	return finalResponse, nil
}
func UnlikedNews(UserId string,newsId string)([]byte,error){
	fmt.Print("nnews",newsId)
	var loginInfo []models.UserSignup
	db:=system.UDB.C(system.USER_COLLECTION)
	err:=db.Find(bson.M{"_id":UserId,"status":system.ACTIVE}).All(&loginInfo)
	if err!=nil{
		return nil,err
	}
	fmt.Print("datataaaa   ",loginInfo)
	unlikedNews:=make(map[string]interface{})
	if len(loginInfo)==0{
		return nil,system.InvalidLoginErr
	}else{
		for i:=0;i<len(loginInfo[0].NewsUnLiked);i++{
			unlikedNews[loginInfo[0].NewsUnLiked[i]]=i
		}

	}
	if unlikedNews[newsId] !=nil{
		if unlikedNews[newsId].(int)==0{
			loginInfo[0].NewsUnLiked=loginInfo[0].NewsUnLiked[1:]
		}else{
			after:=loginInfo[0].NewsUnLiked[unlikedNews[UserId].(int)+1:]
			loginInfo[0].NewsUnLiked=loginInfo[0].NewsUnLiked[0:unlikedNews[UserId].(int)]
			loginInfo[0].NewsUnLiked=append(loginInfo[0].NewsUnLiked,after...)
		}
	}else{
		loginInfo[0].NewsUnLiked=append(loginInfo[0].NewsUnLiked,newsId)
	}
	where:=bson.M{"_id":UserId}
	update:=bson.M{"$set":bson.M{"newsUnLinked":loginInfo[0].NewsUnLiked}}
	err=db.Update(where,update)
	if err!=nil{
		return nil,err
	}
	response:=make(map[string]interface{})
	response["message"]="data updated "
	finalResponse, _ := json.Marshal(response)
	return finalResponse, nil
}