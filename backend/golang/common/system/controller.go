package system

import (
	"reflect"
	"github.com/zenazn/goji/web"
	"net/http"
	"fmt"
	"encoding/json"
	"gopkg.in/mgo.v2/bson"
	"strings"
)

type Application struct {
}
type Controller struct {
}


func(application *Application) Routes(controllerAccess interface{},controllerName string,urlAccesstype string)interface{}{
	fn:=func(c web.C, w http.ResponseWriter, r *http.Request) {

		if urlAccesstype=="private"{
			w.Header().Set("Content-Type", "application/json")
			w.Header().Set("Access-Control-Allow-Origin", "*")
			AccessKey:=r.Header["Authorization"]
			fmt.Println("accesskey ",AccessKey)
			authToken := strings.Split(AccessKey[0], "bearer")[1]
			fmt.Println("accesskey ",authToken)
			key :=RemoveSpace(authToken)
			if ValidateUser(key)==false{
				w.WriteHeader(http.StatusUnauthorized)
				response := make(map[string]interface{})
				response["message"] = "invalid Access"
				errResponse, _ := json.Marshal(response)
				w.Write(errResponse)
				return
			}

		}
		fmt.Println("controllerName ",controllerName)
		methodValue := reflect.ValueOf(controllerAccess).MethodByName(controllerName)
		fmt.Println("controllerName000000000000",methodValue)
		methodInterface := methodValue.Interface()
		fmt.Println("controllerName11111111111111 ",methodInterface)
		method := methodInterface.(func(c web.C, w http.ResponseWriter, r *http.Request) ([]byte, error))
		fmt.Println("controllerName22222222222 ",method)
		result, err := method(c, w, r)
		if err !=nil{
			w.Header().Set("Access-Control-Allow-Origin", "*")
			response := make(map[string]interface{})
			if IsFunctionalError(err){
				w.WriteHeader(http.StatusPreconditionFailed)
				response["message"]=err.Error()
			}else{
				response["message"] = InternalServerError.Error()
				w.WriteHeader(http.StatusInternalServerError)
			}
			errResponse, _ := json.Marshal(response)
			w.Write(errResponse)
		}
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.WriteHeader(http.StatusOK)
		w.Write(result)

	}
	return fn
}

func ValidateUser(userId string)bool{
	var loginInfo []map[string]interface{}
	db:=UDB.C(USER_COLLECTION)
	//checking user already exist or not
	err=db.Find(bson.M{"_id":userId,"status":ACTIVE}).All(&loginInfo)
	if err!=nil{
		return false
	}
	if len(loginInfo)>0{
		return true
	}
	return false
}