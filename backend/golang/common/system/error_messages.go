package system
import "errors"

var (
	InternalServerError = errors.New("Sorry, Something went wrong.")
	InvalidLoginErr = errors.New("invalid Credential")
	NameBlankErr = errors.New("Name can't be blank")
	MobileBlankErr = errors.New("Mobile number can't be blank")
	PasswordBlankErr = errors.New("Password  can't be blank")
	UserAlreadyExistErr = errors.New("User alreday registered")
	NoDataFoundErr = errors.New("NO data Found")

)

func GetErrorMessagesMap() (map[error]bool) {

	errorMessageMap := map[error]bool{
		InvalidLoginErr:true,
		NameBlankErr:true,
		MobileBlankErr:true,
		PasswordBlankErr:true,
		UserAlreadyExistErr:true,
		NoDataFoundErr:true,
	}
	return errorMessageMap
}
func IsFunctionalError(err error) bool {
	errorMessageMap := GetErrorMessagesMap()

	if (errorMessageMap[err]) {
		return true
	}
	return false
}