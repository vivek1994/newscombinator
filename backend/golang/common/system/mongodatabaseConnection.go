package system

import (
	"gopkg.in/mgo.v2"
	"regexp"
)
const (
	MongoServerAddr = "localhost"
)
var (
	MongoSession, err = mgo.Dial(MongoServerAddr)

	MDB  = MongoSession.DB("message")
	MCol = MDB.C("new")
	MSav = MDB.C("save")

	UDB  = MongoSession.DB("newscombinator")
)
func RemoveSpace(name string) string {
	re_leadclose_whtsp := regexp.MustCompile(`^[\s\p{Zs}]+|[\s\p{Zs}]+$`)
	re_inside_whtsp := regexp.MustCompile(`[\s\p{Zs}]{2,}`)

	final := re_leadclose_whtsp.ReplaceAllString(name, "")
	final1 := re_inside_whtsp.ReplaceAllString(final, " ")
	return final1
}
